﻿using System.Windows;
using System.Windows.Forms;
using System.Windows.Controls;
using SampleCreator.Models;

namespace SampleCreator.Views
{
    /// <summary>
    /// Главное окно программы
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Инициализация окна
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обработчик события нажатия кнопки загрузки изображений
        /// Открывает окно выбора директории с изображениями 
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие</param>
        /// <param name="e">Параметры события</param>
        private void OpenFolderClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            if (dialog.SelectedPath != string.Empty)
            {
                FolderPathTextBox.Text = dialog.SelectedPath;
                var items = ImageLoader.Load(dialog.SelectedPath);
                ImagesListView.ItemsSource = items;
                if (items.Count == 0)
                    System.Windows.MessageBox.Show("Указанная директория не содержит изображений");
            }
        }

        /// <summary>
        /// Обработчик выбора изображения из списка
        /// Вызывает метод предобработки выделенного изображения
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие</param>
        /// <param name="e"></param>
        private void ImageSelectionChanged(object sender, SelectionChangedEventArgs e) =>
            ProcessSelectedImage();

        /// <summary>
        /// Обработчик нажатия кнопки обновления предобработанного изображения
        /// Если в данный момент выбрано какое-нибудь изображение
        /// То вызывает метод его бинаризации/фильтрации заново
        /// </summary>
        /// <param name="sender">Объект, вызвавший событие</param>
        /// <param name="e">Параметры события</param>
        private void UpdateProcessedImageClick(object sender, RoutedEventArgs e)
        {
            if (ImagesListView.SelectedItem == null)
                System.Windows.MessageBox.Show("Не выбрано изображение для предобработки");
            else
                ProcessSelectedImage();
        }

        /// <summary>
        /// Предобработка выбранного изображения
        /// </summary>
        private void ProcessSelectedImage()
        {
            var image = ((SampleItem)ImagesListView.SelectedItem).OriginalImage;
            var preprocessedImage = ImageProcessor.Preprocess(
                image,
                ConsiderSdCheckBox.IsChecked == true,
                UseFilterCheckBox.IsChecked == true,
                (int)FilterSizeNumericUpDown.Value);
            PreprocessedImage.Source = BitmapToBitmapImageConverter.Convert(preprocessedImage);
        }
    }
}