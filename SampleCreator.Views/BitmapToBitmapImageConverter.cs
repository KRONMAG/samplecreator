﻿using System;
using System.IO;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using SampleCreator.Models;

namespace SampleCreator.Views
{
    /// <summary>
    /// Конвертер битового изображения
    /// </summary>
    class BitmapToBitmapImageConverter : IValueConverter
    {
        /// <summary>
        /// Конвертация Bitmap-изображения в BitmapSource
        /// </summary>
        /// <param name="bitmap">Битовое изображение</param>
        /// <returns>Преобразованное изображение</returns>
        public static BitmapSource Convert(Bitmap bitmap)
        {
            var stream = new MemoryStream();
            bitmap.Save(stream, ImageFormat.Bmp);
            var image = new BitmapImage();
            image.BeginInit();
            stream.Seek(0, SeekOrigin.Begin);
            image.StreamSource = stream;
            image.EndInit();
            return image;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(((SampleItem)value).ThumbnailImage);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}