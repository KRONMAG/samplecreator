﻿using System.Collections.Generic;
using SQLite;

namespace PriceTagApp.Models
{
    public class Repository<T> where T : new()
    {
        private readonly SQLiteConnection _connection;

        public Repository()
        {
            _connection = new SQLiteConnection("data.db");
            _connection.CreateTable<T>();
        }

        public void Update(T item) =>
            _connection.Update(item);

        public void Add(T item) =>
            _connection.Insert(item);

        public T Get(int id)
        {
            try
            {
                return _connection.Get<T>(id);
            }
            catch
            {
                return default(T);
            }
        }

        public IEnumerable<T> Get() =>
            _connection.Table<T>();

        public void Delete(int id) =>
            _connection.Delete<T>(id);
    }
}