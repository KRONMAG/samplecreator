﻿using System.Drawing;

namespace SampleCreator.Models
{
    /// <summary>
    /// Элемент выборки
    /// </summary>
    public class SampleItem
    {
        /// <summary>
        /// Исходное изображение
        /// </summary>
        public Bitmap OriginalImage { get; set; }

        /// <summary>
        /// Миниатюра изображения
        /// </summary>
        public Bitmap ThumbnailImage { get; set; }
    }
}