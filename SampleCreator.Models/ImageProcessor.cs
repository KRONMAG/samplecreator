﻿using System;
using System.Drawing;
using System.Diagnostics;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;

namespace SampleCreator.Models
{
    /// <summary>
    /// Реализация алгоритма выделения контура объекта
    /// </summary>
    public static class ImageProcessor
    {
        /// <summary>
        /// Предобработка изображения: бинаризация, применение медианного фильтра
        /// </summary>
        /// <param name="bitmap">Исходное изображение</param>
        /// <param name="considerSd">
        /// Требуется ли учитывать стандартное отклонение значений цветов, яркости изображения
        /// при вычислении порога бинаризации
        /// </param>
        /// <param name="useFilter">Следует ли использовать медианный фильтр</param>
        /// <param name="filterSize">Размер фильтра - нечетное целое число</param>
        /// <returns>Преобразованное изображение</returns>
        public static Bitmap Preprocess(Bitmap bitmap, 
                                        bool considerSd = false,
                                        bool useFilter = false,
                                        int filterSize = 9)
        {
            Debug.Assert(bitmap != null);
            Debug.Assert(!useFilter || filterSize > 0 && (filterSize & 1) == 1);
            var mean = new MCvScalar();
            var sd = new MCvScalar();
            var image = new Image<Bgr, int>(bitmap);
            CvInvoke.MeanStdDev(image, ref mean, ref sd);
            double GetBrightness(MCvScalar scalar) =>
                0.11 * scalar.V0 + 0.59 * scalar.V1 + 0.3 * scalar.V2;
            mean.V3 = GetBrightness(mean);
            var imageData = image.Data;
            var threshold = mean;
            if (considerSd)
            {
                Parallel.For(0, image.Rows, i =>
                {
                    for (var j = 0; j < image.Cols; j++)
                    {
                        var scalar = new MCvScalar(imageData[i, j, 0],
                                                   imageData[i, j, 1],
                                                   imageData[i, j, 2]);
                        sd.V3 += Math.Pow(mean.V3 - GetBrightness(scalar), 2);
                    }
                });
                sd.V3 = Math.Sqrt(sd.V3 / (image.Rows * image.Cols - 1));
                threshold = new MCvScalar(mean.V0 - sd.V0, mean.V1 - sd.V1,
                                          mean.V2 - sd.V2, mean.V3 - sd.V3);
            }
            var binImage = new Image<Gray, byte>(image.Width, image.Height);
            binImage.SetValue(new Gray(0));
            var binImageData = binImage.Data;
            bool IsAllGreater(MCvScalar colors) =>
                colors.V0 > threshold.V0 &&
                colors.V1 > threshold.V1 &&
                colors.V2 > threshold.V2 &&
                colors.V3 > threshold.V3;
            Parallel.For(0, binImage.Rows, i =>
            {
                for (var j = 0; j < image.Cols; j++)
                {
                    var scalar = new MCvScalar(imageData[i, j, 0],
                                               imageData[i, j, 1],
                                               imageData[i, j, 2]);
                    scalar.V3 = GetBrightness(scalar);

                    if (IsAllGreater(scalar))
                        binImageData[i, j, 0] = 255;
                }
            });
            if (useFilter)
                binImage = binImage.SmoothMedian(filterSize);
            return binImage.Bitmap;
        }
    }
}