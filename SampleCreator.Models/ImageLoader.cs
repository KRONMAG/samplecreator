﻿using System;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SampleCreator.Models
{
    /// <summary>
    /// Загрузчик изображений
    /// </summary>
    public static class ImageLoader
    {
        /// <summary>
        /// Поиск и загрузка изображений из исходной директории и ее поддиректорий
        /// </summary>
        /// <param name="folderPath">Путь к каталогу с изображениями</param>
        /// <returns>Загруженные изображения, их эскизы</returns>
        public static IList<SampleItem> Load(string folderPath)
        {
            Debug.Assert(folderPath != null);
            Debug.Assert(Directory.Exists(folderPath));
            var imagePaths = new List<string>(Directory.GetFiles(folderPath, "*.jpg"));
            void GetPathsFromSubfolders(string parentFolderPath)
            {
                foreach (var path in Directory.GetDirectories(parentFolderPath))
                {
                    Directory.GetFiles(path, "*.jpg")
                        .ToList()
                        .ForEach(imagePath => imagePaths.Add(imagePath));
                    GetPathsFromSubfolders(path);
                }
            }
            GetPathsFromSubfolders(folderPath);
            var items = new List<SampleItem>();
            Parallel.ForEach(imagePaths, imagePath =>
                {
                   var originalImage = new Bitmap(imagePath);
                   var height = 320;
                   var width = (int)(originalImage.Width * (height / (double)originalImage.Height));
                   items.Add(new SampleItem()
                   {
                       OriginalImage = originalImage,
                       ThumbnailImage = new Bitmap(originalImage.GetThumbnailImage(width, height, null, IntPtr.Zero))
                   });
                });
            return items;
        }
    }
}